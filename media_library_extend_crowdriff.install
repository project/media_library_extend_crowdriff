<?php

/**
 * @file
 * media_library_extend_crowdriff.install
 */

/**
 * Implements hook_requirements().
 */
function media_library_extend_crowdriff_requirements($phase): array {
  return [];
}

/**
 * Implements hook_install().
 *
 * @see hook_install()
 */
function media_library_extend_crowdriff_install() {
  $config = \Drupal::configFactory()->getEditable('entity_usage.settings');
  if ($config && !$config->isNew()) {
    $local_task_enabled_entity_types = $config->get('local_task_enabled_entity_types');
    if (!is_array($local_task_enabled_entity_types)) {
      $local_task_enabled_entity_types = [];
    }
    if (!in_array('media', $local_task_enabled_entity_types)) {
      $local_task_enabled_entity_types[] = 'media';
    }

    $track_enabled_source_entity_types = $config->get('track_enabled_source_entity_types');
    if (!is_array($track_enabled_source_entity_types)) {
      $track_enabled_source_entity_types = [];
    }
    if (!in_array('node', $track_enabled_source_entity_types)) {
      $track_enabled_source_entity_types[] = 'node';
    }

    $track_enabled_target_entity_types = $config->get('track_enabled_target_entity_types');
    if (!is_array($track_enabled_target_entity_types)) {
      $track_enabled_target_entity_types = [];
    }
    if (!in_array('media', $track_enabled_target_entity_types)) {
      $track_enabled_target_entity_types[] = 'media';
    }

    $track_enabled_plugins = $config->get('track_enabled_plugins');
    if (!is_array($track_enabled_plugins)) {
      $track_enabled_plugins = [];
    }
    if (!in_array('entity_reference', $track_enabled_plugins)) {
      $track_enabled_plugins[] = 'entity_reference';
    }
    if (!in_array('media_embed', $track_enabled_plugins)) {
      $track_enabled_plugins[] = 'media_embed';
    }

    $edit_warning_message_entity_types = $config->get('edit_warning_message_entity_types');
    if (!is_array($edit_warning_message_entity_types)) {
      $edit_warning_message_entity_types = [];
    }
    if (!in_array('media', $edit_warning_message_entity_types)) {
      $edit_warning_message_entity_types[] = 'media';
    }

    $delete_warning_message_entity_types = $config->get('delete_warning_message_entity_types');
    if (!is_array($delete_warning_message_entity_types)) {
      $delete_warning_message_entity_types = [];
    }
    if (!in_array('media', $delete_warning_message_entity_types)) {
      $delete_warning_message_entity_types[] = 'media';
    }

    $config
      ->set('local_task_enabled_entity_types', $local_task_enabled_entity_types)
      ->set('track_enabled_source_entity_types', $track_enabled_source_entity_types)
      ->set('track_enabled_target_entity_types', $track_enabled_target_entity_types)
      ->set('track_enabled_plugins', $track_enabled_plugins)
      ->set('track_enabled_base_fields', FALSE)
      ->set('edit_warning_message_entity_types', $edit_warning_message_entity_types)
      ->set('delete_warning_message_entity_types', $delete_warning_message_entity_types)
      ->save();
  }
}
