<?php

/**
 * @file
 * Contains media_library_extend_crowdriff.views.inc.
 */

/**
 * Implements hook_views_data_alter().
 */
function media_library_extend_crowdriff_views_data_alter(array &$data) {
  $data['media']['asset_usage'] = [
    'title' => t('Asset Usage'),
    'group' => t('Content'),
    'field' => [
      'title' => t('Asset usage'),
      'help' => t('Returns asset usage count for Crowdriff media types.'),
      'id' => 'asset_usage',
    ],
  ];
}
