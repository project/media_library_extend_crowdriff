(function ($, Drupal) {
  Drupal.behaviors.media_library_extend_crowdriff.media_library = {
    attach: function (context, settings) {
      // No results messaging.
      let $media_library_extend_form = $('#media-library-extend-form-content');
      let total_items = 0;
      $('.js-media-library-item:visible', $media_library_extend_form).each(function () {
        if ($(this).hasClass('js-click-to-select')) {
          total_items++;
        }
      });
      $('.media-library__no-assets', $media_library_extend_form).remove();
      if (total_items) {
        $('.js-media-library-extend-pager', $media_library_extend_form).show();
      } else {
        $media_library_extend_form.prepend('<p class="media-library__no-assets">' + Drupal.t('No assets found. Please try adjusting filters.') + '</p>');
        $('.js-media-library-extend-pager', $media_library_extend_form).hide();
      }

      // Check for max and current selections.
      let currentSelection = Drupal.MediaLibrary.currentSelection;
      // Current number of selections matches remaining selections.
      // Disable any further selections.
      if (settings.media_library.selection_remaining && settings.media_library.selection_remaining === currentSelection.length) {
        let $form = $('.js-media-library-views-form, .js-media-library-add-form', context);
        if (!$form.length) {
          return;
        }
        // Get media items that are not checked. Disable remaining.
        let $mediaItems = $('.js-media-library-item input[type="checkbox"]', $form);
        $mediaItems.not(':checked').prop('disabled', true).closest('.js-media-library-item').addClass('media-library-item--disabled');
      }
    }
  };
}(jQuery, Drupal));
