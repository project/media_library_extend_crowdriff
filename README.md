# Media Library Crowdriff

Module provides plugins that extend Media library to pull in
Crowdriff assets as media entities. Provides custom media entities
for Crowdriff images and videos.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_library_extend_crowdriff).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/media_library_extend_crowdriff).


## Required

- Drupal 9.3+ or 10.
- Crowdriff API Key/Token is required.


## Required Modules

- Drupal Media Core
- [Crowdriff API](https://www.drupal.org/project/crowdriff_api)
- [Entity Usage](https://www.drupal.org/project/entity_usage)
- [Field Group](https://www.drupal.org/project/field_group)
- [Media Library Extend](https://www.drupal.org/project/media_library_extend)


## Optional/Noted Modules

**Media File Delete**

Useful module for removing files attached to media entities. Deleting a
media entity by default doesn't remove the files attached.

- [media_file_delete](https://www.drupal.org/project/media_file_delete)

**Queue UI**

Provides UI for viewing and managing queues. Can easily see what's in
the Crowdriff asset refresh queue and also run queue from admin interface.

- [queue_ui](https://www.drupal.org/project/queue_ui)


## Composer Setup

Require package/module:

```bash
composer require drupal/media_library_extend_crowdriff
```


## Installation/Setup

Install and enable module via drush.

```bash
drush en media_library_extend_crowdriff
drush cr
```

General settings and API key can be managed here:
`admin/config/media/crowdriff`

Media panes can be configured here if needed:
`admin/config/media/media-library/pane`

For Entity usage configuration, module provides configuration to report and
track usage on Media entities.

Entity usage settings can be managed here:
`admin/config/entity-usage/settings`


## Uninstall

Crowdriff media entities need to be removed before module can be uninstalled.
On the uninstall page: `admin/modules/uninstall`, user will be warned as such
and given the option to delete media entities.

Any imported configuration by the module will also be removed. User will also
be prompted/warned as such when uninstalling the module.

Media fields referencing Crowdriff media types, should also be updated prior
to uninstallation.

Crowdriff media types will be removed after module is uninstalled.


## General Usage/Setup

**Setup Crowdriff API Key**

Crowdriff API key will need to be configured here before continuing:
`admin/config/services/crowdriff`

**Setup Crowdriff Caching/Cron**

Cron and caching settings can be configured from here:
`admin/config/media/crowdriff`

Caching is turned on by default and is useful for storing API responses based
on specified filter criteria. This helps reduce the number of hits against the
API service.

Cron can be turned on or disabled depending on your needs. Keep in mind, that
refreshing assets will require this setting to be activated. Assets are added
to a queue for processing and are not real time. Queues can be managed
manually through Queue UI module or via drush commands.

**Setup Crowdriff Media Field**

Here are some basic steps to setup a general media field, which uses Crowdriff
media types.

- Select entity or node, add new `"Media"` field.
- Specify your limit or unlimited.
- Under `"Media type"`, select `"Crowdriff Image"` & `"Crowdriff Video"`. Or just one
  depending on your needs.
- Under `"Form Display"`, make sure field is using `"Media Library"` as widget.
- Now when adding new `entity/content`, click on `"Add Media"` button and you should
  see Media library panes such as `"Remote Crowdriff Images"` or
  `"Remote Crowdriff Videos"`. These panes will only appear when associated media
  type is enabled on media field.
- Crowdriff assets will be listed under these panes and are available to pull
  down as a media entity.

**Media Overview**
Crowdriff media overview is provided and can be accessed here:
`admin/content/crowdriff` or under "Content".

This page provides an overview of the Crowdriff media assets currently pulled
down and stored in Drupal.

Assets in this view can be queued up for refreshing via the action or through
the entity operation. Crowdriff media entity edit pages also include a button,
which will also queue up the asset for refreshing.

**Missing Assets View**
Crowdriff missing assets view is provided and can be accessed here:
`admin/content/crowdriff/missing-assets`.

This view essentially filters out Crowdriff media that has been flagged as not
being found on the Crowdriff API service anymore. When this case occurs, a
flag on the media entity is set to indicate this specific case.

Not sure how often this case will occur, but probably not often.

**Unused Assets View**
Crowdriff unused assets view is provided and can be accessed here:
`admin/content/crowdriff/unused-assets`.

This view essentially filters out Crowdriff media that has an entity usage
count of 0 or is empty. Entity usage module provides the media tracking and
usage data. With this view, one can determine which assets are not in use or
referenced by any content on the site.

If media entity is not referenced, it's most likely safe to delete.

**Drush Commands**

```bash
crowdriff:
crowdriff:delete-assets    Delete Crowdriff asset media entities.
crowdriff:refresh-asset    Queue up Crowdriff asset for refreshing.
```

**Queue Crowdriff asset:**

Queue up Crowdriff asset for refreshing.

First parameter is the asset uuid and second parameter is the media type.
Valid media types:
- crowdriff_image
- crowdriff_video

```bash
drush crowdriff:refresh-asset 1234-ig-17859403040727004 crowdriff_image
```

**Run Crowdriff asset refresh queue (crowdriff_asset_refresh):**

Run the Crowdriff asset refresh queue.

```bash
drush queue:list
drush queue:run crowdriff_asset_refresh
```

**Delete Crowdriff media entities (assets):**

This command should be used cautiously. This will remove all Crowdriff media
entities.

```bash
drush crowdriff:delete-assets
```


## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
