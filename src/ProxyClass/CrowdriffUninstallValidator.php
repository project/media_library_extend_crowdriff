<?php

namespace Drupal\media_library_extend_crowdriff\ProxyClass;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Module uninstall validator proxy service class.
 */
class CrowdriffUninstallValidator implements ModuleUninstallValidatorInterface {

  use DependencySerializationTrait;

  /**
   * The proxy service.
   *
   * @var string
   */
  protected $drupalProxyService;

  /**
   * The lazy loaded proxy service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffUninstallValidator
   */
  protected $service;

  /**
   * The service container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructs a ProxyClass Drupal proxy object.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   * @param string $drupal_proxy_service
   *   The service ID of the original service.
   */
  public function __construct(ContainerInterface $container, $drupal_proxy_service) {
    $this->container = $container;
    $this->drupalProxyService = $drupal_proxy_service;
  }

  /**
   * Lazy loads the service from the container.
   *
   * @return object
   *   Returns the constructed service.
   */
  protected function lazyLoad() {
    if (!isset($this->service)) {
      $this->service = $this->container->get($this->drupalProxyService);
    }
    return $this->service;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    return $this->lazyLoad()->validate($module);
  }

  /**
   * {@inheritdoc}
   */
  public function setStringTranslation(TranslationInterface $translation) {
    return $this->lazyLoad()->setStringTranslation($translation);
  }

}
