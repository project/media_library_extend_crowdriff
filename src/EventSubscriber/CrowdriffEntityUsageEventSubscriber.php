<?php

namespace Drupal\media_library_extend_crowdriff\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\entity_usage\Events\EntityUsageEvent;
use Drupal\entity_usage\Events\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Crowdriff Entity Usage Event Subscriber.
 *
 * @package Drupal\media_library_extend_crowdriff\EventSubscriber
 */
class CrowdriffEntityUsageEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents(): array {
    return [
      Events::USAGE_REGISTER => 'invalidateMedia',
      Events::BULK_DELETE_DESTINATIONS => 'invalidateMedia',
      Events::BULK_DELETE_SOURCES => 'invalidateMedia',
      Events::DELETE_BY_FIELD => 'invalidateMedia',
      Events::DELETE_BY_SOURCE_ENTITY => 'invalidateMedia',
      Events::DELETE_BY_TARGET_ENTITY => 'invalidateMedia',
    ];
  }

  /**
   * Invalidate Media cache tag on specific entity usage events.
   *
   * @param \Drupal\entity_usage\Events\EntityUsageEvent $event
   *   The entity usage event.
   */
  public function invalidateMedia(EntityUsageEvent $event) {
    if (!empty($event->getTargetEntityType()) && $event->getTargetEntityType() == 'media') {
      if ($mediaId = $event->getTargetEntityId()) {
        Cache::invalidateTags(['media:' . $mediaId]);
      }
    }
  }

}
