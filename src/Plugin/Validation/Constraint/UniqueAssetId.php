<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the Crowdriff asset id is unique/not duplicated.
 *
 * @Constraint(
 *   id = "UniqueAssetId",
 *   label = @Translation("Unique Crowdriff Asset Id", context = "Validation"),
 *   type = "string"
 * )
 */
class UniqueAssetId extends Constraint {

  /**
   * The message that will be shown if the value is not unique.
   *
   * @var string
   */
  public $message = 'The Crowdriff asset %value, is not unique.';

}
