<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\Media;
use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Drupal\crowdriff_api\CrowdriffService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Refreshes queued up Crowdriff assets.
 *
 * @QueueWorker (
 *   id = "crowdriff_asset_refresh",
 *   title = @Translation("Crowdriff Asset Refresh"),
 *   cron = {"time" = 30}
 * )
 */
class CrowdriffAssetRefresh extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Drupal entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The Crowdriff service.
   *
   * @var \Drupal\crowdriff_api\CrowdriffService
   */
  protected $crowdriffService;

  /**
   * The Crowdriff asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelInterface $loggerChannel,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactory $config_factory,
    CrowdriffService $crowdriff_service,
    CrowdriffAssetService $crowdriff_asset_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerChannel = $loggerChannel;
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
    $this->crowdriffService = $crowdriff_service;
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('media_library_extend_crowdriff'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('crowdriff_api.crowdriff_service'),
      $container->get('media_library_extend_crowdriff.crowdriff_asset_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): bool {
    // Make sure we have media entity id.
    if (empty($data['media_id'])) {
      $this->loggerChannel->error('Unable to process Crowdriff asset. Media entity id is missing.');
      return FALSE;
    }

    // Make sure we have Crowdriff asset uuid.
    if (empty($data['asset_id'])) {
      $this->loggerChannel->error('Unable to process Crowdriff asset. Asset id is missing.');
      return FALSE;
    }

    // Set variables.
    $asset_id = $data['asset_id'];
    $media_id = $data['media_id'];

    // Load up media entity.
    /** @var \Drupal\media\Entity\Media $entity */
    if ($entity = $this->entityTypeManager->getStorage('media')->load($media_id)) {

      // Determine if media type is image or video.
      $bundle = $entity->bundle();

      // Check for valid bundle.
      if (!in_array($bundle, ['crowdriff_image', 'crowdriff_video'])) {
        $this->loggerChannel->warning('Invalid entity bundle @bundle.', ['@bundle' => $bundle]);
        return FALSE;
      }

      // Let's fetch Crowdriff asset.
      if ($asset = $this->crowdriffAssetService->getAsset($asset_id)) {

        // Check to see if asset changed. If not, we will stop here.
        if (!$this->crowdriffAssetService->isAssetChanged($asset, $entity)) {
          return FALSE;
        }

        // Set common asset fields.
        $entity = $this->crowdriffAssetService->setCommonAssetFields($asset, $entity);

        // Get source field.
        $source_field = $this->crowdriffAssetService->getAssetSourceField($entity->bundle());

        // Specific logic for crowdriff_image bundle.
        if ($bundle == 'crowdriff_image') {
          // Save the asset image.
          $entity = $this->saveSourceAssetImage($source_field, $asset, $entity);
        }

        // Specific logic for crowdriff_video bundle.
        if ($bundle == 'crowdriff_video') {
          // Save the asset video.
          $entity = $this->saveSourceAssetVideo($source_field, $asset, $entity);

          // Save the asset image.
          $secondary_source_field = 'field_media_image';
          $entity = $this->saveSourceAssetImage($secondary_source_field, $asset, $entity);
        }

        // Save media entity.
        if ($entity->save()) {
          return TRUE;
        }
        else {
          $this->loggerChannel->error('Failed to save Crowdriff asset @uuid', ['@uuid' => $asset_id]);
        }
      }
      else {
        $this->loggerChannel->warning('Failed to find Crowdriff asset @uuid', ['@uuid' => $asset_id]);

        // Mark asset exists to false.
        if ($entity->hasField('field_crowdriff_asset_exists')) {
          $entity->set('field_crowdriff_asset_exists', FALSE);
        }

        // Save media entity.
        if ($entity->save()) {
          return TRUE;
        }
        else {
          $this->loggerChannel->error('Failed to save Crowdriff media entity @id', ['@id' => $media_id]);
        }
      }
    }
    else {
      $this->loggerChannel->error('Failed to find Crowdriff asset media entity @id', ['@id' => $media_id]);
    }

    return FALSE;
  }

  /**
   * Save asset image for specified source field.
   *
   * @param string $source_field
   *   The asset source field.
   * @param array $asset
   *   The asset.
   * @param \Drupal\media\Entity\Media $entity
   *   The media entity.
   *
   * @return \Drupal\media\Entity\Media
   *   Returns the media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveSourceAssetImage(string $source_field, array $asset, Media $entity): Media {
    // Source field check (image file).
    if (!empty($source_field) && $entity->hasField($source_field) && !$entity->get($source_field)->isEmpty()) {

      // Get file id for asset image.
      $asset_image_fid = $entity->get($source_field)->getValue()[0]['target_id'];
      /** @var \Drupal\file\Entity\File $asset_image_file */
      $asset_image_file = $this->entityTypeManager->getStorage('file')->load($asset_image_fid);
      $asset_image_filesize = $asset_image_file->getSize();

      // Compare local filesize with remote filesize.
      // If same, we leave it alone.
      // If not the same, we download asset file and update.
      if ($asset_image_filesize && !empty($asset['image_original']['size'])) {

        // Sizes don't match.
        if ($asset_image_filesize != $asset['image_original']['size']) {

          // Get upload location.
          $upload_location = $this->crowdriffAssetService->getAssetUploadLocation($entity->bundle());
          if ($fid = $this->crowdriffAssetService->downloadAssetFile($asset['image_original'], $asset['uuid'], $upload_location)) {
            $media_image = $entity->get($source_field)->getValue();
            $media_image[0]['target_id'] = $fid;
            $entity->set($source_field, $media_image);
          }
          else {
            $this->loggerChannel->error('Failed to update Crowdriff asset source image @uuid', ['@uuid' => $asset['uuid']]);
          }
        }
      }
    }

    return $entity;
  }

  /**
   * Save asset video for specified source field.
   *
   * @param string $source_field
   *   The asset source field.
   * @param array $asset
   *   The asset.
   * @param \Drupal\media\Entity\Media $entity
   *   The media entity.
   *
   * @return \Drupal\media\Entity\Media
   *   Returns the media entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveSourceAssetVideo(string $source_field, array $asset, Media $entity): Media {
    // Source field check (video file).
    if (!empty($source_field) && $entity->hasField($source_field) && !$entity->get($source_field)->isEmpty()) {

      // Get file id for asset video.
      $asset_video_fid = $entity->get($source_field)->getValue()[0]['target_id'];
      /** @var \Drupal\file\Entity\File $asset_video_file */
      $asset_video_file = $this->entityTypeManager->getStorage('file')->load($asset_video_fid);
      $asset_video_filesize = $asset_video_file->getSize();

      // Compare local filesize with remote filesize.
      // If same, we leave it alone.
      // If not the same, we download asset file and update.
      if ($asset_video_filesize && !empty($asset['video_original']['size'])) {

        // Sizes don't match.
        if ($asset_video_filesize != $asset['video_original']['size']) {

          // Get upload location.
          $upload_location = $this->crowdriffAssetService->getAssetUploadLocation($entity->bundle());
          if ($fid = $this->crowdriffAssetService->downloadAssetFile($asset['video_original'], $asset['uuid'], $upload_location)) {
            $media_video = $entity->get($source_field)->getValue();
            $media_video[0]['target_id'] = $fid;
            $entity->set($source_field, $media_video);
          }
          else {
            $this->loggerChannel->error('Failed to update Crowdriff asset source video @uuid', ['@uuid' => $asset['uuid']]);
          }
        }
      }
    }

    return $entity;
  }

}
