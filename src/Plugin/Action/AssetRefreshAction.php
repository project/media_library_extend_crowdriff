<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\media\MediaInterface;
use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crowdriff Asset Refresh Action.
 *
 * @Action(
 *   id = "asset_refresh_action",
 *   label = @Translation("Refresh Crowdriff Asset"),
 *   type = "media"
 * )
 */
class AssetRefreshAction extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The Crowdriff asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\media_library_extend_crowdriff\CrowdriffAssetService $crowdriff_asset_service
   *   The Crowdriff asset service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CrowdriffAssetService $crowdriff_asset_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('media_library_extend_crowdriff.crowdriff_asset_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($media = NULL) {
    if ($media instanceof MediaInterface) {
      $valid_bundles = [
        'crowdriff_image',
        'crowdriff_video',
      ];
      if (in_array($media->bundle(), $valid_bundles)) {
        if (!$media->get('field_crowdriff_asset_uuid')->isEmpty()) {
          if ($asset_uuid = $media->get('field_crowdriff_asset_uuid')->value) {
            $status = $this->crowdriffAssetService->queueAsset($media->id(), $asset_uuid);
            if (!$status) {
              $this->messenger()->addError($this->t('Failed to add asset @id to the queue for refresh.', ['@id' => $asset_uuid]));
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (!$account->hasPermission('sync crowdriff asset')) {
      return FALSE;
    }
    /** @var \Drupal\media\MediaInterface $object */
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
