<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\views\field;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\media\MediaInterface;
use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler for Crowdriff asset usage.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("asset_usage")
 */
class AssetUsage extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Crowdriff asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * The constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\media_library_extend_crowdriff\CrowdriffAssetService $crowdriff_asset_service
   *   The Crowdriff asset service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CrowdriffAssetService $crowdriff_asset_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('media_library_extend_crowdriff.crowdriff_asset_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $usage_count = 0;

    // Get entity.
    $entity = $this->getEntity($values);
    if ($entity instanceof MediaInterface) {

      $cacheableMetaData = new CacheableMetadata();

      if ($entity->bundle() == 'crowdriff_image' || $entity->bundle() == 'crowdriff_video') {
        $cacheableMetaData->addCacheableDependency($entity);
        $cache_tags = [];
        // Get sources nested, so we can get correct usage count.
        $sources_nested = $this->crowdriffAssetService->getAssetUsage($entity);
        foreach ($sources_nested as $source_items) {
          $usage_count += count($source_items);
        }
        // Get sources not nested. See to apply proper cache tags.
        $sources_not_nested = $this->crowdriffAssetService->getAssetUsage($entity, FALSE);
        foreach ($sources_not_nested as $source_item) {
          $cache_tag = $source_item['source_type'] . ':' . $source_item['source_id'];
          if (!in_array($cache_tag, $cache_tags)) {
            $cache_tags[] = $cache_tag;
          }
        }
        // Add cache tags.
        $cacheableMetaData->addCacheTags($cache_tags);
      }

      $url = Url::fromRoute('entity.media.entity_usage', [
        'media' => $entity->id(),
      ]);

      // Build link and apply cache metadata.
      $link = Link::fromTextAndUrl($usage_count, $url)->toRenderable();
      $cacheableMetaData->applyTo($link);

      // Return rendered link.
      return $this->renderer->render($link);
    }

    return NULL;
  }

}
