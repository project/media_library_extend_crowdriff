<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\media\Plugin\media\Source\Image;
use Drupal\crowdriff_api\CrowdriffService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crowdriff Image entity media source.
 *
 * @see \Drupal\Core\Image\ImageInterface
 *
 * @MediaSource(
 *   id = "crowdriff_image",
 *   label = @Translation("Crowdriff Image"),
 *   description = @Translation("Use Crowdriff image assets for reusable media."),
 *   allowed_field_types = {"image"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   thumbnail_alt_metadata_attribute = "thumbnail_alt_value"
 * )
 */
class CrowdriffImage extends Image {

  /**
   * The Crowdriff service.
   *
   * @var \Drupal\crowdriff_api\CrowdriffService
   */
  protected $crowdriffService;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\crowdriff_api\CrowdriffService $crowdriff_service
   *   The crowdriff service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    ImageFactory $image_factory,
    FileSystemInterface $file_system,
    CrowdriffService $crowdriff_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory, $image_factory, $file_system);
    $this->crowdriffService = $crowdriff_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('image.factory'),
      $container->get('file_system'),
      $container->get('crowdriff_api.crowdriff_service')
    );
  }

}
