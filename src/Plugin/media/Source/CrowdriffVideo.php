<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\Plugin\media\Source\VideoFile;

/**
 * Crowdriff Video entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "crowdriff_video",
 *   label = @Translation("Crowdriff Video"),
 *   description = @Translation("Use Crowdriff video assets for reusable media."),
 *   allowed_field_types = {"file"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class CrowdriffVideo extends VideoFile {

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    /** @var \Drupal\file\FileInterface $file */
    $file = $media->get($this->configuration['source_field'])->entity;

    // If the source field is not required, it may be empty.
    if (!$file) {
      return parent::getMetadata($media, $attribute_name);
    }

    // Get metadata.
    switch ($attribute_name) {
      // Get thumbnail.
      case 'thumbnail_uri':
        if ($media->hasField('field_media_image') && !$media->get('field_media_image')->isEmpty()) {
          if ($asset_image_fid = $media->get('field_media_image')->getValue()[0]['target_id']) {
            $asset_image_file = $this->entityTypeManager->getStorage('file')->load($asset_image_fid);
            return $asset_image_file->getFileUri();
          }
        }
        return parent::getMetadata($media, $attribute_name);

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

}
