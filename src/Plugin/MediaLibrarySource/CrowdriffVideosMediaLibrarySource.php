<?php

namespace Drupal\media_library_extend_crowdriff\Plugin\MediaLibrarySource;

use Drupal\media_library_extend_crowdriff\CrowdriffMediaLibrarySourceBase;

/**
 * Provides a media library pane to pull in Crowdriff video assets.
 *
 * @MediaLibrarySource(
 *   id = "crowdriff_videos",
 *   label = @Translation("Crowdriff Videos"),
 *   source_types = {
 *     "crowdriff_video",
 *     "video_file",
 *   },
 * )
 */
class CrowdriffVideosMediaLibrarySource extends CrowdriffMediaLibrarySourceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'media_type' => 'video',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId($selected_id) {
    if ($this->configuration['debug']) {
      $this->crowdriffService->getLogger()
        ->debug('Selected id: ' . $selected_id);
    }

    // Check to see if media entity for asset already exists by uuid.
    if ($entity_id = $this->crowdriffAssetService->assetExists($selected_id, $this->getTargetBundle())) {
      // Return existing media entity id.
      return $entity_id;
    }

    // Lookup asset by selected uuid.
    if ($asset = $this->crowdriffAssetService->getAsset($selected_id)) {

      if ($this->configuration['debug']) {
        $this->crowdriffService->getLogger()->debug(print_r($asset, TRUE));
      }

      // Create a media entity stub.
      $entity = $this->createEntityStub($selected_id);

      // Check media type.
      $media_type = $this->configuration['media_type'];
      if ($media_type == $asset['media_type']) {

        // Initialize asset file.
        $asset_file = NULL;

        // Attempt to get the best video from asset.
        if (!empty($asset['video_original']['url'])) {
          $asset_file = $asset['video_original'];
        }

        // Make sure we have asset file.
        if ($asset_file) {

          // Save to filesystem.
          if ($video_fid = $this->crowdriffAssetService->downloadAssetFile($asset_file, $selected_id, $this->getUploadLocation())) {

            // Attach video file to media entity.
            $source_field = $this->getSourceField();

            // Set video file target id (fid).
            $entity->{$source_field}->target_id = $video_fid;

            // Set asset image.
            if (!empty($asset['image_original']['url'])) {
              if ($image_fid = $this->crowdriffAssetService->downloadAssetFile($asset['image_original'], $selected_id, $this->getUploadLocation())) {
                $asset_image_data = [
                  'target_id' => $image_fid,
                  'alt' => '',
                  'title' => '',
                ];
                $entity->set('field_media_image', $asset_image_data);
              }
            }

            // Set asset uuid (uuid).
            if ($entity->hasField('field_crowdriff_asset_uuid')) {
              $entity->set('field_crowdriff_asset_uuid', $selected_id);
            }

            // Set common asset fields.
            $entity = $this->crowdriffAssetService->setCommonAssetFields($asset, $entity);

            // Save media entity.
            if ($entity->save()) {
              return $entity->id();
            }
            else {
              $this->messenger()
                ->addError($this->t('Failed to save Crowdriff asset: @uuid', ['@uuid' => $selected_id]));
              $this->crowdriffService->getLogger()
                ->error('Failed to save media entity for Crowdriff asset.');
            }
          }
          else {
            $this->crowdriffService->getLogger()
              ->error('Failed to download Crowdriff source asset file.');
          }
        }
        else {
          $this->messenger()
            ->addWarning($this->t('Unable to find Crowdriff asset file: @uuid', ['@uuid' => $selected_id]));
          $this->crowdriffService->getLogger()
            ->warning('Failed to find Crowdriff asset file.');
        }
      }
      else {
        $this->crowdriffService->getLogger()
          ->warning('Crowdriff asset media type is a mismatch or not valid.');
      }
    }
    else {
      $this->messenger()
        ->addWarning($this->t('Unable to find Crowdriff asset: @uuid', ['@uuid' => $selected_id]));
      $this->crowdriffService->getLogger()
        ->warning('Failed to find Crowdriff asset: ' . $selected_id);
    }

    // Return NULL if we reach this point.
    return NULL;
  }

}
