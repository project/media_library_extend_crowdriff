<?php

namespace Drupal\media_library_extend_crowdriff\Form;

use Drupal\Core\Cache\CacheFactoryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Crowdriff Config Form.
 *
 * @package Drupal\media_library_extend_crowdriff\Form
 */
class CrowdriffConfigForm extends ConfigFormBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * CrowdriffConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheFactoryInterface $cache_factory
   *   The cache factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheFactoryInterface $cache_factory) {
    parent::__construct($config_factory);
    $this->cache = $cache_factory->get('media_library_extend_crowdriff');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_library_extend_crowdriff.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_library_extend_crowdriff_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('media_library_extend_crowdriff.settings');

    $form['cron'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron Settings'),
    ];

    $form['cron']['sync'] = [
      '#title' => $this->t('Enable syncing with Crowdriff API.'),
      '#description' => $this->t('Enable or disable cron process to sync/refresh assets with Crowdriff API.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('sync'),
    ];

    $form['cron']['sync_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Asset refresh interval'),
      '#options' => [
        '-1' => $this->t('Every cron run'),
        '3600' => $this->t('Every hour'),
        '7200' => $this->t('Every 2 hours'),
        '10800' => $this->t('Every 3 hours'),
        '14400' => $this->t('Every 4 hours'),
        '21600' => $this->t('Every 6 hours'),
        '28800' => $this->t('Every 8 hours'),
        '43200' => $this->t('Every 12 hours'),
        '86400' => $this->t('Every 24 hours'),
      ],
      '#default_value' => empty($config->get('sync_interval')) ? 3600 : $config->get('sync_interval'),
      '#description' => $this->t('How often should Crowdriff assets be synced/refreshed with Crowdriff API?'),
      '#states' => [
        'visible' => [
          ':input[name="sync"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    // Clear cache.
    $this->cache->invalidateAll();
    // Set config.
    $this->config('media_library_extend_crowdriff.settings')
      ->set('sync', $form_state->getValue('sync'))
      ->set('sync_interval', $form_state->getValue('sync_interval'))
      ->save();
  }

}
