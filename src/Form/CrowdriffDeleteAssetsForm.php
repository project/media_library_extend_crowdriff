<?php

namespace Drupal\media_library_extend_crowdriff\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to delete Crowdriff assets.
 */
class CrowdriffDeleteAssetsForm extends ConfirmFormBase {

  use RedirectDestinationTrait;

  /**
   * The Crowdriff asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * The constructor.
   *
   * @param \Drupal\media_library_extend_crowdriff\CrowdriffAssetService $crowdriff_asset_service
   *   The Crowdriff asset service.
   */
  public function __construct(CrowdriffAssetService $crowdriff_asset_service) {
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_library_extend_crowdriff.crowdriff_asset_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->crowdriffAssetService->deleteAssets();
    $this->messenger()->addMessage($this->t('All Crowdriff assets deleted successfully.'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "crowdriff_delete_assets_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.modules_uninstall');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Crowdriff assets will be deleted permanently. Please proceed cautiously.');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete all Crowdriff media entities (assets)?');
  }

}
