<?php

namespace Drupal\media_library_extend_crowdriff\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to confirm asset refresh.
 */
class CrowdriffAssetRefreshForm extends ConfirmFormBase {

  use RedirectDestinationTrait;

  /**
   * Media entity id.
   *
   * @var int
   */
  protected $mediaId;

  /**
   * Crowdriff asset id.
   *
   * @var string
   */
  protected $assetId;

  /**
   * The Crowdriff asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * The constructor.
   *
   * @param \Drupal\media_library_extend_crowdriff\CrowdriffAssetService $crowdriff_asset_service
   *   The Crowdriff asset service.
   */
  public function __construct(CrowdriffAssetService $crowdriff_asset_service) {
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_library_extend_crowdriff.crowdriff_asset_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $media_id = NULL, string $asset_id = NULL) {
    $this->mediaId = $media_id;
    $this->assetId = $asset_id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->mediaId) && !empty($this->assetId)) {
      $status = $this->crowdriffAssetService->queueAsset($this->mediaId, $this->assetId);
      if (!$status) {
        $this->messenger()->addError($this->t('Failed to add asset @id to the queue for refresh.', ['@id' => $this->assetId]));
      }
      else {
        $this->messenger()->addMessage($this->t('Asset @id was added to refresh queue successfully.', ['@id' => $this->assetId]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "crowdriff_asset_refresh_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.media.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Crowdriff asset %id will be added to queue for refreshing. Asset and media entity will be updated accordingly.', ['%id' => $this->assetId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to refresh Crowdriff asset %id?', ['%id' => $this->assetId]);
  }

}
