<?php

namespace Drupal\media_library_extend_crowdriff\Commands;

use Drupal\media_library_extend_crowdriff\CrowdriffAssetService;
use Drush\Commands\DrushCommands;

/**
 * Crowdriff drush commands.
 */
class CrowdriffCommands extends DrushCommands {

  /**
   * The Crowdriff Asset service.
   *
   * @var \Drupal\media_library_extend_crowdriff\CrowdriffAssetService
   */
  protected $crowdriffAssetService;

  /**
   * {@inheritdoc}
   */
  public function __construct(CrowdriffAssetService $crowdriff_asset_service) {
    parent::__construct();
    $this->crowdriffAssetService = $crowdriff_asset_service;
  }

  /**
   * Delete Crowdriff asset media entities.
   *
   * @command crowdriff:delete-assets
   */
  public function deleteAssets() {
    if ($this->io()->confirm('Are you sure you want to delete all Crowdriff media entities?', FALSE)) {
      try {
        $this->crowdriffAssetService->deleteAssets();
        $this->output()->writeln('Crowdriff asset media entities deleted.');
      }
      catch (\Exception $e) {
        $this->output()->writeln($e->getMessage());
      }
    }
  }

  /**
   * Queue up Crowdriff asset for refreshing.
   *
   * @param array $options
   *   The options array.
   *
   * @command crowdriff:refresh-asset
   * @usage crowdriff:refresh-asset --uuid=2883-ig-1008129687225905408 --bundle=crowdriff_image
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function queueAsset(array $options = [
    'uuid' => NULL,
    'bundle' => 'crowdriff_image',
  ]) {
    $uuid = $options['uuid'];
    $bundle = $options['bundle'];
    if (!empty($uuid) && $media_id = $this->crowdriffAssetService->assetExists($uuid, $bundle)) {
      $this->output()->writeln('Crowdriff asset media entity ' . $media_id . ' found.');
      if ($this->crowdriffAssetService->queueAsset($media_id, $uuid)) {
        $this->output()->writeln('Crowdriff asset ' . $uuid . ' queued for refreshing.');
      }
    }
  }

}
